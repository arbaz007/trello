import "./App.css";
import axios from "axios";
import Boards from "./components/Boards";
import { Route, Routes } from "react-router-dom";
import { useEffect, useState } from "react";
import Board from "./components/Board";
import Navbar from "./components/Navbar";
function App() {
  return (
    <>
      <Navbar />
      <Routes>
        <Route path="boards" element={<Boards />} />
        <Route path="board/:id" element={<Board />} />
      </Routes>
    </>
  );
}

export default App;
