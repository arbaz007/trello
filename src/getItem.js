import axios from "axios";

const apiKey = import.meta.env.VITE_API_KEY;
const token = import.meta.env.VITE_TOKEN;

const createItem = async (parentID, parentName, input, itemName) => {
  try {
    if (!input || input.trim() === "") {
      throw new Error("please provide an input");
    }
    let list = await axios.post(`https://api.trello.com/1/${itemName}`, {
      name: input,
      [parentName]: parentID,
      key: apiKey,
      token: token,
    });
    list = list.data;
    return list;
  } catch (err) {
    console.log("error occured: ", err.message);
  }
};
const getItem = async (parent, parentID, item, setItem = false) => {
  try {
    let data = await axios.get(
      `https://api.trello.com/1/${parent}/${parentID}/${item}?key=${apiKey}&token=${token}`
    );
    if (!data) {
      throw new Error("error while fetching lists data");
    }
    data = data.data;
    if (setItem) {
      setItem(data);
    }
  } catch (err) {
    console.log("error occured: ", err.message);
  }
};

const deleteItem = async (item, itemID, setItems, items) => {
  let data = await axios.delete(
    `https://api.trello.com/1/${item}/${itemID}?key=${apiKey}&token=${token}`,
    {
      headers: {
        Accept: "application/json",
      },
    }
  );
  setItems(items.filter((elem) => elem?.id !== itemID));
  console.log(`${item} deleted`);
};

const calldeleteCheckItem = async (parentID, idCheckItem, setItems, items) => {
  try {
    let data = await axios.delete(
      `https://api.trello.com/1/checklists/${parentID}/checkItems/${idCheckItem}?key=${apiKey}&token=${token}`
    );
    if (!data) {
      throw new Error("error while fetching checkItem data");
    }
    data = data.data;
    console.log(data);
    setItems(items.filter((item) => item?.id !== idCheckItem));
  } catch (err) {
    console.log("Error occured: ", err.message);
  }
};

export { getItem, deleteItem, calldeleteCheckItem, createItem };
