import { AppBar, Box, Button, Toolbar, Typography } from "@mui/material";

let Navbar = () => {
  return (
    <>
      <Box sx={{ flexGrow: 1}}>
        <AppBar position="static" sx={{background: "#06283D"}}>
          <Toolbar >
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              Trello
            </Typography>
            <Button color="inherit">Boards</Button>
          </Toolbar>
        </AppBar>
      </Box>
    </>
  );
};
export default Navbar
