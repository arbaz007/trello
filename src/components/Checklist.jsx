import { Box, Button, Modal, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import AddComp from "./AddComp";
import { Delete, Token } from "@mui/icons-material";
import axios from "axios";
import CheckItem from "./CheckItem";
import { calldeleteCheckItem, getItem } from "../getItem";
import LinearProgress, { linearProgressClasses } from '@mui/material/LinearProgress';
import { styled } from '@mui/material/styles';

const Checklist = ({ checklist, deleteCheckList, idCard }) => {
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;
  const [checkItems, setCheckItems] = useState([]);
  const [input, setInput] = useState("");
  const [progressValue, setProgressValue] = useState(0)
  let totalCheckItem = checklist?.checkItems.length;
  let totalChecked = checklist?.checkItems.filter(each => each?.state === "complete").length
  let avgChecked = (totalChecked / totalCheckItem)*100
  useEffect(()=> {
    setProgressValue(avgChecked)
  }, [avgChecked])
  console.log(totalCheckItem);
  console.log(totalChecked);
  console.log(avgChecked);
  useEffect(() => {
    const getCheckItems = async () => {
      getItem("checklists", checklist?.id, "checkItems", setCheckItems);
    };
    getCheckItems();
  }, []);
  const deleteCheckItem = async (idCheckItem) => {
    calldeleteCheckItem(checklist?.id, idCheckItem, setCheckItems, checkItems);
  };
  const createCheckItem = async () => {
    try {
      let data = await axios.post(
        `https://api.trello.com/1/checklists/${checklist?.id}/checkItems?name=${input}&key=${apiKey}&token=${token}`
      );
      if (!data) {
        throw new Error("error while create checkItem");
      }
      data = data.data;
      return data;
    } catch (err) {
      console.log("Error occured: ", err.message);
    }
  };
  const addCheckItem = async () => {
    const newCheckItem = await createCheckItem();
    console.log("before: ", checkItems);
    if (input !== "") {
      setCheckItems((prev) => [...prev, newCheckItem]);
    }
    console.log("after: ", checkItems);
  };
  const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
    height: 2,
    width: "100%",
    borderRadius: 5,
    [`&.${linearProgressClasses.colorPrimary}`]: {
      backgroundColor: theme.palette.grey[theme.palette.mode === 'light' ? 200 : 800],
    },
    [`& .${linearProgressClasses.bar}`]: {
      borderRadius: 5,
      backgroundColor: theme.palette.mode === 'light' ? '#1a90ff' : '#308fe8',
    },
  }));
  return (
    <>
      <Box>
        <Box sx={{ display: "flex" }}>
          <Typography
            key={checklist?.id}
            style={{
              marginRight: "auto",
              fontSize: "1.3rem",
              marginBottom: "1rem",
            }}
          >
            {checklist?.name}
          </Typography>
          <Delete
            onClick={() => deleteCheckList(checklist?.id)}
            sx={{ color: "#FF204E" }}
          />
        </Box>
        <Box>
          {checkItems.map((checkitem) => {
            return (
              <CheckItem
                key={checkitem?.id}
                checkitem={checkitem}
                deleteCheckItem={deleteCheckItem}
                idCard={idCard}
              />
            );
          })}
          <BorderLinearProgress variant="determinate" value={progressValue} />
          <AddComp
            name={"Check Item"}
            addComp={addCheckItem}
            parentId={checklist?.id}
            input={input}
            setInput={setInput}
          />
        </Box>
      </Box>
    </>
  );
};
export default Checklist;
