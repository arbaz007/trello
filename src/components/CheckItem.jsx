import { Delete } from "@mui/icons-material";
import { Box, Checkbox, FormControlLabel, Typography } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";



const CheckItem = ({ checkitem, deleteCheckItem, idCard }) => {
  const [checked, setChecked] = useState("");
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;

  useEffect(() => {
    const getCheckItemState = async () => {
      try {
        let data = await axios.get(
          `https://api.trello.com/1/cards/${idCard}/checkItem/${checkitem?.id}`,
          {
            params: {
              key: apiKey,
              token: token,
            },
          }
        );
        if (!data) {
          throw new Error("error while getting checkitem state");
        }
        data = data.data;
        setChecked(data.state);
      } catch (err) {
        console.log("Error occured: ", err.message);
      }
    };
    getCheckItemState();
  }, []);

  const checkItemState = async (checked) => {
    try {
      let data = await axios.put(
        `https://api.trello.com/1/cards/${idCard}/checkItem/${checkitem?.id}`,
        {
          key: apiKey,
          token: token,
          state: checked,
        }
      );
      if (!data) {
        throw new Error("error while updating state of checkitem");
      }
      data = data.data;
      console.log(data);
      setChecked(data.state);
    } catch (err) {
      console.log("Error occured: ", err.message);
    }
  };
  
  
  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        padding: "0 0.8rem",
        background: "#EEF5FF",
        borderRadius: "0.5rem",
        marginBottom: "0.5rem",
      }}
    >
      <FormControlLabel
        sx={{ margin: "0" }}
        control={
          <Checkbox
            checked={checked === "complete" ? true : false}
            onChange={(e) => {
              if (e.target.checked) {
                checkItemState("complete");
              } else {
                checkItemState("incomplete");
              }
            }}
          />
        }
      />
      <Typography variant="p" component={"p"} sx={{ marginRight: "auto" }}>
        {checkitem?.name}
      </Typography>
      <Delete onClick={() => deleteCheckItem(checkitem?.id)} sx={{color: "#FF204E"}}/>
      
    </Box>
  );
};
export default CheckItem;
