import Checklist from "./Checklist";
import axios from "axios";
import { useEffect, useState } from "react";
import { Box, Modal, ListItem, Typography, Divider } from "@mui/material";
import { Delete } from "@mui/icons-material";
import AddComp from "./AddComp";
import { createItem, deleteItem, getItem } from "../getItem";

const Card = ({ card, deleteCard }) => {
  const [input, setInput] = useState("");
  const [open, setOpen] = useState(false);
  const [checklists, setCheckLists] = useState([]);
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    boxShadow: 24,
    p: 4,
    borderRadius: "0.5rem",
  };
  useEffect(() => {
    const getCheckLists = async () => {
      getItem("cards", card?.id, "checklists", setCheckLists);
    };
    getCheckLists();
  }, []);

  const deleteCheckList = async (id) => {
    deleteItem("checklists", id, setCheckLists, checklists);
  };

  const addCheckList = async () => {
    const createdCheckLIst = await createItem(
      card?.id,
      "idCard",
      input,
      "checklists"
    );
    setCheckLists((prev) => [...prev, createdCheckLIst]);
  };
  return (
    <>
      <ListItem
        key={card?.id}
        sx={{
          padding: "0.5rem",
          marginBottom: "0.5rem",
          border: "1px solid rgb(247, 247, 247, 0.7)",
          borderRadius: "0.5rem",
        }}
      >
        <Typography sx={{ marginRight: "auto" }} onClick={() => setOpen(true)}>
          {card?.name}
        </Typography>

        <Delete
          sx={{
            color: "#FF204E",
            fontSize: "1.3rem",
            "&:hover": { cursor: "pointer" },
          }}
          onClick={() => deleteCard(card?.id)}
        />

        <Modal
          open={open}
          onClose={() => setOpen(false)}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            {checklists.map((checklist) => {
              return (
                <Checklist
                  checklist={checklist}
                  addCheckList={addCheckList}
                  open={open}
                  setOpen={setOpen}
                  input={input}
                  setInput={setInput}
                  idCard={card?.id}
                  deleteCheckList={deleteCheckList}
                />
              );
            })}
            <Divider />
            <Box sx={{ marginTop: "1rem" }} />
            <Box sx={{margin: "0 auto", width: "fit-content"}}>
              <AddComp
                name={"checkList"}
                addComp={addCheckList}
                parentId={card?.id}
                input={input}
                setInput={setInput}
              />
            </Box>
          </Box>
        </Modal>
      </ListItem>
    </>
  );
};
export default Card;
