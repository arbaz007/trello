import axios from "axios";
import { useState } from "react";
import AddComp from "./AddComp";
import { Box, Button, Input } from "@mui/material";

const CreateBoard = ({allBoards, setAllBoards}) => {
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;
  let createBoard = async () => {
    let newBoard = await axios.post(
      `https://api.trello.com/1/boards/?name=${input}&key=${apiKey}&token=${token}`
    );
    console.log(newBoard.status);
    console.log(newBoard.data);
    console.log("new board is created");
    newBoard = newBoard.data
    return newBoard
  };
  const [showInput, setShowInput] = useState(false);
  const [input, setinput] = useState("");

  const handleSubmit =  async (e) => {
    e.preventDefault();
    setShowInput(false);
    setinput("");
    let newBoard = await createBoard();
    setAllBoards(prev => [...prev, newBoard])
    console.log(allBoards);
  };
  return (
    <Box sx={{width:"150px"}}>
      {!showInput && <Button sx={{width: "100%", height:"100%"}} variant="contained" onClick={() => setShowInput(true)}>Create</Button>}
      {showInput && (
        <form onSubmit={handleSubmit}>
          <Input
            type="text"
            placeholder="Enter value"
            value={input}
            onChange={e => setinput(e.target.value)}
          />
          <Button variant="contained" type="submit">Submit</Button>
        </form>
      )}
    </Box>
  );
};
export default CreateBoard