import styled from "@emotion/styled";
import { Delete, Edit } from "@mui/icons-material";
import { Box, LinearProgress, List, ListItem, Typography } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import AddComp from "./AddComp";
import Card from "./Card";
import { createItem, deleteItem, getItem } from "../getItem";
let Lists = ({ list, idList, deleteList }) => {
  const [input, setInput] = useState("");
  const [cards, setCards] = useState([]);
  const [loader, setLoader] = useState(true);
  useEffect(() => {
    const getCards = async () => {
      getItem("lists", idList, "cards", setCards);
      setLoader(false);
    };
    getCards();
  }, []);

  const deleteCard = async (idCard) => {
    deleteItem("cards", idCard, setCards, cards);
  };
  const editCard = async (idCard) => {
    console.log(idCard);
  };
  const addCard = async (idList) => {
    if (input !== "") {
      const newCard = await createItem(idList, "idList", input, "cards");
      setCards((prev) => [...prev, newCard]);
    }
  };
  const Header = styled("p")(() => {
    return {
      fontSize: "2rem",
      margin: "0",
    };
  });

  return (
    <Box
      key={list?.id}
      sx={{
        backdropFilter: "blur(16px) saturate(180%)",
        backgroundColor: "rgba(255, 255, 255, 0.5)",
        borderRadius: "12px",
        border: "1px solid rgba(209, 213, 219, 0.3)",
        padding: "1rem",
        width: "250px",
      }}
    >
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          gap: "1rem",
          marginBottom: "0.8rem",
        }}
      >
        <Header>{list?.name}</Header>
        <Delete
          onClick={() => deleteList(list?.id)}
          sx={{ color: "#FF204E" }}
        />
      </Box>
      {loader && (
        <Box sx={{ width: "100%" }}>
          <LinearProgress />
        </Box>
      )}
      {!loader && (
        <List>
          {cards.map((card) => (
            <Card
              key={card?.id}
              card={card}
              deleteCard={deleteCard}
              editCard={editCard}
            />
          ))}
          <AddComp
            name={"card"}
            addComp={addCard}
            parentId={list?.id}
            input={input}
            setInput={setInput}
          />
        </List>
      )}
    </Box>
  );
};
export default Lists;
