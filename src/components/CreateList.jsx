import { AddBox } from "@mui/icons-material";
import { Button, Input, FormLabel } from "@mui/material";
import axios from "axios";
import { useState } from "react";

const CreateList = ({ setLists, id }) => {
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;
  const [showInput, setShowInput] = useState(false);
  const [input, setInput] = useState("");
  const createlist = async (id) => {
    try {
      if (!input || input.trim() === "") {
        throw new Error("please provide an input");
      }
      let list = await axios.post(`https://api.trello.com/1/lists`, {
        name: input,
        idBoard: id,
        key: apiKey,
        token: token,
      });
      list = list.data;
      console.log(list);
      setLists((prev) => [...prev, list]);
    } catch (err) {
      console.log("error occured: ", err.message);
    }
    console.log("list is created");
  };


  const addList = (id) => {
    setShowInput(false);
    createlist(id);
    setInput("")
  };

  return (
    <>
      {!showInput && (
        <Button onClick={() => setShowInput(true)}>
          Create List
          <AddBox />
        </Button>
      )}
      {showInput && (
        <FormLabel>
          <Input
            value={input}
            onChange={(e) => setInput(e.target.value)}
          ></Input>
          <Button onClick={() => addList(id)}>Add </Button>
        </FormLabel>
      )}
    </>
  );
};
export default CreateList;
